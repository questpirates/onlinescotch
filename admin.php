<?php
include("connection.php");
session_start();
$connect = $con;

if ($_SESSION['IsLoggedin'] == 'trueAdmin') {

    if (filter_input(INPUT_GET, 'action') == 'logout') {
        $_SESSION['IsLoggedin'] = 'false';
        header('location: mainpage.php');
    }
} 
else {
    header('location: mainpage.php');
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist\css\bootstrap.min.css"/>
    <link rel="stylesheet" href="mainpage.css"/>
    <link rel="stylesheet" href="font-awesome-4.7.0\css\font-awesome.min.css"/>
    <script src="jquery.min.js"></script>
    <script src="bootstrap-3.3.7-dist\js\bootstrap.min.js"></script>
    <script src="sweetalert.min.js"></script>
</head>
<body style="background-color:rgb(232,232,232);width:100%">

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" style="margin-left:2em" href="#">Scotch Hub</a>
            </div>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Admin</a></li>
            <li style="margin-right:5em"><a href="admin.php?action=logout"><span class="glyphicon glyphicon-log-out"></span> &nbsp;Logout</a></li>
        </ul>
        <ul class="navbar-form ">
            <div class="input-group">
            <input type="text" class="form-control" style="height:30px;width:500px;margin-top:3px" placeholder="Search for products" name="search">
            <div class="input-group-btn">
                <a class="btn btn-default" href="home.php" name="navSearch" style="height:30px;margin-top:3px">
                <i class="glyphicon glyphicon-search"></i>
                </a>
            </div>
            </div>
        </ul>
        </div>
    </nav>

        <div class="col-md-1"></div>
        <div class="col-md-10">
          <div class="panel panel-default">
            <div class="panel-heading">
            
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#menu1">Products List</a></li>
                <li><a data-toggle="tab" href="#menu2">Add Product</a></li>
            </ul>
            
            </div>
            <div class="panel-body">

            <div class="tab-content">

                <div id="menu1" class="tab-pane fade in active">
                    
                    <?php
                        $query = "SELECT * FROM products order by Id DESC";
                        $result = mysqli_query($connect, $query);
                        $count = mysqli_num_rows($result);
                        if ($count > 0):
                    ?>
                
                <table class="table" id="staticParent" name="staticParent" style="width:80%;margin-left:10%">
                    <tr>
                        <th width="30%">Product</th>
                        <th width="25%">Product Name</th>
                        <th width="15%" class="text-center">Price</th>
                        <th width="15%" class="text-center">In Stock</th>
                        <th width="15%" class="text-center">Action</th>
                    </tr>


                <?php
                    while ($product = mysqli_fetch_assoc($result)) {
                ?>

                    <tr>
                        <td>
                            <img src="data:image/jpeg;base64, <?php echo base64_encode($product['Image']); ?>" name="image" style="width:100px;height:100px" />
                        </td>
                        <td><?php echo $product['Name']; ?></td>
                        <td class="text-center">₹ <?php echo $product['Price']; ?></td>
                        <td class="text-center"><?php echo $product['InStock']; ?></td>
                        <td class="text-center">
                        <div class="row">
                        <div class="col-md-3" style="margin-top:2px;margin-left:17%">
                            <button type="button" data-toggle="modal" data-target="#editModal<?php echo $product['Id']; ?>" name="btnEdit" id="btnEdit" class="btn-link">
                                <a ><i class="fa fa-edit" style="font-size:21px" title="Edit"></i></a>
                            </button>
                        </div>
                        <div class="col-md-3">
                            <button type="button" data-id="<?php echo $product['Id']; ?>" name="btnDelete" id="btnDelete" class="btn-link">
                                <a><i class="fa fa-trash" style="font-size:21px" title="Delete"></i></a>
                            </button>
                        </div>
                        </div>
                        <td>
                    </tr>

        <!--Edit Product Modal -->
		<div id="editModal<?php echo $product['Id']; ?>" class="modal fade" role="dialog">
		  <div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Product Details</h4>
			  </div>
			  <div class="modal-body">
				<form method="post" enctype="multipart/form-data" id="editproduct_form" name="editproduct_form" class="editForm">
					<div class="form-group">
						<label for="pname2">Name</label>
						<input type="hidden" value="<?php echo $product["Id"]; ?>" name="prodId">
						<input type="text" class="form-control" value="<?php echo $product["Name"]; ?>" name="pname" id="pname2" required>
					</div>
					<div class="form-group">
						<label for="price2">Price</label>
                        <input type="text" class="form-control" value="<?php echo $product["Price"]; ?>" name="price" id="price2" required>
					</div>
					<!-- <div class="form-group">
						<label for="image2">Image</label>
						<input type="file" class="form-control" name="image" id="image2" required>
					</div> -->
					<div class="form-group">
						<label for="instock2">In Stock</label>
						<input type="number" class="form-control" value="<?php echo $product["InStock"]; ?>" name="instock" id="instock2" required>
					</div>
					<div class="form-group">
						<button type="submit" name="btnUpdateProduct" class="btn btn-info"
						style="width:7em;margin-left:40%">Update</button> <!-- data-dismiss="modal" data-toggle="modal" data-target="#myModal" -->
					</div>
				</form>
			  </div>
			  <div class="modal-footer">
			  </div>
			</div>

		  </div>
		</div>


                    <?php
                        }
                    ?>
                </table>

                    <?php
                    else: ?>
                    <h4 class="text-center">Products table is empty</h4>
                    <!-- <a href="home.php" style="margin-left:40%;width:20%" class="btn btn-info col-md-6"><i class="fa fa-cart-plus" ></i>&nbsp; Add Items Now </a> -->
                    <?php
                    endif;?>
                    
                </div>

                <div id="menu2" class="tab-pane fade">
                    
                <h4 class="text-center">Add Product Details</h4>
                <form method="post" enctype="multipart/form-data" id="addproduct_form" name="addproduct_form">
                    <div class="form-group row">
                        <div class="col-md-2"></div>
                        <div class="col-md-2 text-right">
                            <label for="pname" style="margin-top:5px">Name</label>
                        </div>
                        <div class="col-md-5">
                            <input type="text" name="pname" id="pname" class="form-control" placeholder="Enter product name" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-2"></div>
                        <div class="col-md-2 text-right">
                            <label for="price" style="margin-top:5px">Price</label>
                        </div>
                        <div class="col-md-5">
                            <input type="text" name="price" id="price" class="form-control" placeholder="Enter product price" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-2"></div>
                        <div class="col-md-2 text-right">
                            <label for="image" style="margin-top:5px">Image</label>
                        </div>
                        <div class="col-md-5">
                            <input type="file" name="image" id="image" class="form-control" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-2"></div>
                        <div class="col-md-2 text-right">
                            <label for="instock" style="margin-top:5px">In Stock</label>
                        </div>
                        <div class="col-md-5">
                            <input type="number" name="instock" id="instock" class="form-control" placeholder="Enter product in stock" required />
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class=" text-center">
                        <input type="submit" style="width:8em" class="btn btn-info" name="btnAdd" id="btnAdd" value="Add"/>
                        </div>
                    </div>
                </form>

                </div>

                
            </div>

            
            </div>
            </div>
            </div>
            <div class="col-md-1"></div>

<script type="text/javascript">

$(document).ready(function(){
    $('#btnAdd').click(function(){
        var extension = $('#image').val().split('.').pop().toLowerCase();
        if($('#image').val() !='' && jQuery.inArray(extension, ['gif','png','jpg','jpeg']) == -1){
            swal('Invalid Image Formate!','File must be JPG, GIF, JPEG or PNG','warning');
            $('#image').val('');
            return false;
        }
        
        if($('#image').val() !='' && $('#image')[0].files[0].size > 1048576){
            swal('Image is too big!','It should be less than 1MB','warning');
            $('#image').val('');
            return false;
        }


    });

    $('#addproduct_form').on('submit', function(e){
    e.preventDefault();
    //alert('aaaa');
    $.ajax({
        url: "addProduct.php",
        method: "POST",
        data: new FormData($('#addproduct_form')[0]),
        contentType: false,
        processData: false,
        success: function(data)
        {
            if(data == 'saved')
            {
                $('#addproduct_form')[0].reset();
                swal({  title: 'Product added Successfully!',
                        icon: 'success' ,
                }).then(function() {
                window.location = "admin.php";
                });
            }
            else{
                $('#addproduct_form')[0].reset();
                swal({  title: 'Something wrong happened!',
                        text:  data,//'Please try again later!',
                        icon: 'warning' ,
                }).then(function() {
                window.location = "admin.php";
                });
            }
        }
    });
    }); 

    $('#staticParent').on('click', '#btnDelete', function(e){
        e.preventDefault();
        var pid = $(this).attr("data-id");
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this product!",
            icon: "warning",
            buttons: ["No, Cancel!", "Yes, Delete!"],
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {

                $.ajax({
                    url: "deleteProduct.php?pid="+pid,
                    success: function(data)
                    {
                        if(data == 'saved')
                        {
                            swal({  title: 'Product deleted Successfully!',
                                    icon: 'success' ,
                            }).then(function() {
                            window.location = "admin.php";
                            });
                        }
                        else{
                            swal({  title: 'Something wrong happened!',
                                    text: 'Please try again later!',
                                    icon: 'warning' ,
                            }).then(function() {
                            window.location = "admin.php";
                            });
                        }
                    }
                });

            } 
        });

    });

    
    $('.editForm').on('submit', function(e){
    e.preventDefault();
    //alert('aaaa');
    $.ajax({
        url: "updateProduct.php",
        method: "POST",
        data: $(this).serialize(),
        success: function(data)
        {
            if(data == 'saved')
            {
                $('#editproduct_form')[0].reset();
                //$('#myModal2').modal('hide');
                swal({  title: 'Product details updated Successfully!',
                        icon: 'success' ,
                }).then(function() {
                window.location = "admin.php";
                });
            }
            else{
                $('#editproduct_form')[0].reset();
                //$('#myModal2').modal('hide');
                swal({  title: 'Something wrong happened!',
                        text: 'Please try again later',
                        icon: 'warning' ,
                }).then(function() {
                window.location = "admin.php";
                });
            }
        }
    });
    });


});

</script>
</body>
</html>