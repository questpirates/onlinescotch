<?php
include("connection.php");
session_start();
$db = $con;
$_SESSION['isFromCheckout'] = 'no';

if ($_SESSION['IsLoggedin'] == 'true') {
    if (isset($_POST['add_to_cart']) && $_SESSION['msg'] != $_POST) {
        $ProductId = filter_input(INPUT_GET, 'id');
        $UserId = $_SESSION['UserId'];
        $Quantity = mysqli_real_escape_string($db, $_POST['quantity']);

        $query = "SELECT * FROM cart WHERE ProductId = $ProductId AND UserId = $UserId";
        $result = mysqli_query($db, $query);
        if ($result) {
            if (mysqli_num_rows($result) > 0) {
                $row = mysqli_fetch_assoc($result);
                $Quantity = $Quantity + $row["Quantity"];
                $sql = "UPDATE cart SET Quantity = $Quantity WHERE ProductId = $ProductId AND UserId = $UserId";
                mysqli_query($db, $sql);
                header("location:cart.php");
            } else {
                $sql = "INSERT INTO cart (ProductId,UserId,Quantity)
					VALUES('$ProductId','$UserId','$Quantity')";
                mysqli_query($db, $sql);
                header("location:cart.php");
            }
        }
    }

    if (filter_input(INPUT_GET, 'action') == 'logout') {
        $_SESSION['IsLoggedin'] = 'false';
        $_SESSION['UserId'] = '';
        $_SESSION['UserName'] = '';
        header('location: mainpage.php');
    }

} else {
    header('location: mainpage.php');
}

$_SESSION['msg'] = $_POST;

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Shopping cart</title>
		<link rel="stylesheet" href="bootstrap-3.3.7-dist\css\bootstrap.min.css"/>
    <link rel="stylesheet" href="mainpage.css"/>
    <link rel="stylesheet" href="cart.css"/>
		<link rel="stylesheet" href="font-awesome-4.7.0\css\font-awesome.min.css"/>
    <script src="jquery.min.js"></script>
    <script src="bootstrap-3.3.7-dist\js\bootstrap.min.js"></script>
    <script src="sweetalert.min.js"></script>
	</head>
	<body>

		<!-- more_pop_up -->
		  <!-- cusomet care pop up -->
			<div id="myModal_customercare" class="modal fade" role="dialog">
		  <div class="modal-dialog">

							<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Customer Care</h4>
						</div>
							<div class="modal-body">
								<p><b class="text-danger">
										24/7 care
									</b></p>
									<i class="fa fa-phone"></i> &nbsp;9739751689<br/>
									<i class="fa fa-envelope"></i> &nbsp;bmcgroups.scotchhub@gmail.com
							</div>
						</div>

					</div>

		  </div>
			</div>

			<!-- about us pop up -->
			 <!-- cusomet care pop up -->
			 <div id="myModal_aboutus" class="modal fade" role="dialog">
		  <div class="modal-dialog">

							<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">About Us</h4>
						</div>
						<div class="modal-body">
							<div class="content">
								<p>
									<b>Scotch Hub</b> is an initial sample product of <b>bmch group</b>'s IT sector. 
								</p>
							</div>
						</div>

					</div>

		  </div>
	  	</div>


		<nav class="navbar navbar-inverse">
		  <div class="container-fluid">
			<div class="navbar-header">
			  <!--<img src="scotch-glass.png" style="width:50px;height:80px"/>-->
			  <a class="navbar-brand" style="margin-left:1em" href="home.php">Scotch Hub</a>
			</div>
			<ul class="nav navbar-nav navbar-right">
			  <!--<li class="active"><a href="#">Home</a></li>
			  <li><a href="#">Page 1</a></li>
			  <li><a href="#">Page 2</a></li>-->
			  <li class="dropdown">
				<a class="dropdown-toggle" href="#">
					<?php echo $_SESSION['UserName']; ?>
					<i class="fa fa-angle-down"></i>
					<i class="fa fa-angle-up"></i>
				</a>
				<ul class="dropdown-menu">
				  <li><a href="orders.php"><i class="fa fa-shopping-bag"></i> &nbsp;My Orders</a></li>
				  <!-- <li class="divider"></li>
				  <li><a href="#"><i class="fa fa-address-book-o"></i> &nbsp;My Address</a></li> -->
				  <li class="divider"></li>
				  <li><a href="profile.php"><i class="fa fa-user-circle-o"></i> &nbsp;My Profile</a></li>
				  <li class="divider"></li>
				  <li><a href="home.php?action=logout" name="logout"><span class="glyphicon glyphicon-log-out"></span> &nbsp;Logout</a></li>
				</ul>
			  </li>
			  <li>
					<a href="cart.php"><i class="fa fa-shopping-cart" style="color:white"></i> &nbsp;Cart
					<?php
						$connect = $con;
						$uid = $_SESSION['UserId'];
						$query = "SELECT COUNT(*) FROM cart where UserId = $uid";
						$result = mysqli_query($connect, $query);
						$count = mysqli_fetch_array($result);
						if ($count[0] > 0):
					?>
						<span class="badge" style="font-size:11px">
              <?php echo $count[0]; ?>
						</span>
					<?php endif;?>
					</a>
			  </li>
			  <li class="dropdown" style="margin-right:5em">
				<a class="dropdown-toggle" href="#">More
					<i class="fa fa-angle-down"></i>
					<i class="fa fa-angle-up"></i>
				</a>
				<ul class="dropdown-menu">
				  <li><a href="#" data-toggle="modal" data-target="#myModal_customercare"><i class="fa fa-question-circle"></i> &nbsp;Customer Care</a></li>
				  <li class="divider"></li>
				  <li><a href="#" data-toggle="modal" data-target="#myModal_aboutus"><i class="fa fa-address-book-o"></i> &nbsp;About Us</a></li>
				</ul>
			  </li>
			</ul>
			<ul class="navbar-form ">
			  <div class="input-group">
				<input type="text" class="form-control" style="height:30px;width:500px;margin-top:3px" placeholder="Search for products" id="navSearchtb">
				<div class="input-group-btn">
				  <button class="btn btn-default" type="button" id="navSearchbtn" style="height:30px;margin-top:3px">
					<i class="glyphicon glyphicon-search"></i>
				  </button>
				</div>
			  </div>
			</ul>
		  </div>
		</nav>

		<div class="container">
		<div class="scrolling-wrapper">
		<?php
		$query = 'SELECT * FROM products ORDER BY Id DESC';
		$result = mysqli_query($connect, $query);

		if ($result) {
    	if (mysqli_num_rows($result) > 0) {
        while ($product = mysqli_fetch_assoc($result)) {
            //print_r($product);
            ?>
				<div class="col-md-3 col-xs-12 col-sm-6 tr">
				<div class="card">
					<form method="post" action="home.php?action=add&id=<?php echo $product['Id']; ?>">
						 <div class="products">
							<img src="data:image/jpeg;base64, <?php echo base64_encode($product['Image']); ?>" style="width:200px;height:200px;margin-left:1px;" class="img-responsive" />
							<h4 class="text-info"><?php echo $product['Name']; ?></h4>
							<h4>₹ <?php echo $product['Price']; ?></h4>
							<?php
							if ($product['InStock'] > 0) {
                ?>
							<input type="number" name="quantity" class="form-control quantity" value="1" />
							<input type="hidden" name="name" value="<?php echo $product['Name']; ?>" />
							<input type="hidden" name="price" value="<?php echo $product['Price']; ?>" />
							<button type="submit" name="add_to_cart" style="margin-top:5px;" class="btn btn-info">
							<i class="fa fa-shopping-cart" style="color:white"></i> &nbsp;Add to Cart
							</button>
							<?php
								} else {
                ?>
								<h4 class="text-danger" style="margin-top:3rem">Out of stock</h4>
								<br/>
							</button>
							<?php
							}
            	?>
						 </div>
					</form>
				</div>
				</div>
				<?php
				}
						}
				}
				?>
	</div>
	</div>

 <script>
	$(document).ready(function(){
		
		$("#navSearchbtn").on("click", function() {
			var value = $("#navSearchtb").val().toLowerCase();
			$(".scrolling-wrapper .tr").filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		});

	});

</script>

</body>
</html>